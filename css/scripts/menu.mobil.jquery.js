$(".toggle-nav").click(function(e) {
  e.preventDefault();
  $("nav").toggleClass("open").toggleClass("closed");
  $(this).toggleClass("open").toggleClass("closed");
});

$("nav li a").each(function() {
  if ($(this).next("ul").length > 0) {
    $(this).parent().addClass("parent").addClass("closed");
    // insert a click target to show children
    $(this).before('<a href="#more" title="Expand/Collapse" class="more"></a>');
  };
})

$(".more").click(function(e) {
  e.preventDefault();
  $(this).parent().toggleClass("open").toggleClass("closed");
});
