$(document).ready(function() {

//smooth scroll-to-id
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html,body').animate({
              scrollTop: target.offset().top
            }, 800);
            return false;
          }
        }
    });
	
//Scroll to top - Button
var amountScrolled = 400;

$(window).scroll(function() {
  if ( $(window).scrollTop() > amountScrolled ) {
    $('.lck_scroll-top').addClass('show');
  } else {
    $('.lck_scroll-top').removeClass('show');
  }
});

$('.lck_scroll-top').click(function() {
  $('html, body').animate({
    scrollTop: 0
  }, 800);
  return false;
});
		
});
