// Addon (lck) Languagemenu slidedown with jQuery 06/2017
$(document).ready(function() {
 $('.languagemenu a').each(function() {
  var alt = $(this).children('img').attr('alt');
  $(this).append('<span class="languagetext">' + alt + '</span>');
 });

 $('.languagemenu img').wrap("<span class='languageimg'></span>");

 $('.languageselect').hover(
  function() {
   $('.languagemenu', this).stop().slideDown(300);
  },
  function() {
   $('.languagemenu', this).stop().slideUp(200);
  }
 );

});
